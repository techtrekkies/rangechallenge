# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Contains source code for 'Range' challenge

### How do I get set up? ###

** Import as maven project
** RangeHelper is the primary class where the range logic is implemented.

### Assumptions ###
 * This code assumes and made the following choices.
 ** To keep the code simple and cohesive this doesn't assume knowledge on shipping/items etc.
 ** This code was written as a utility that can be re-used anywhere where a similar **range** problem occurs
 ** Not using external libraries, frameworks including logging to keep it simple and reusable.
 ** Using a two pronged approach (sort, find overlaps) to solve the range problem.
 ** Tests are provided - Tests are not intended to be exhaustive
 ** Using Java 8 to take advantage of the language features
 ** Using Java generics and lambda for customization, ease of use, maintability and extensibility.