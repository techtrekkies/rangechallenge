package com.wsi.range.helper;

import com.wsi.common.helper.ListBasedHelper;
import com.wsi.range.compare.RangeComparator;
import com.wsi.range.model.Range;
import com.wsi.range.sort.RangeSorter;

import java.util.*;

/**
 * Assumptions:
 ** To keep the code simple and cohesive this doesn't assume knowledge on shipping/items etc.
 ** This code was written as a utility that can be re-used anywhere where a similar **range** problem occurs
 ** Not using external libraries, frameworks including logging to keep it simple and reusable.
 ** Using a two pronged approach (sort, find overlaps) to solve the range problem.
 ** Tests are provided - Tests are not intended to be exhaustive
 ** Using Java 8 to take advantage of the language features
 ** Using Java generics and lambda for customization, ease of use, maintability and extensibility.
 */
public class RangeHelper implements ListBasedHelper<Range> {

    private static final RangeComparator RANGE_COMPARATOR = new RangeComparator();

    @Override
    public List<Range> handle(final List<Range> givenRanges) {
        if (givenRanges==null) return Collections.EMPTY_LIST;
        if (givenRanges.size()<=1) return givenRanges;
        sortRanges(givenRanges);
        return findOverlappingRanges(givenRanges);
    }

    /**
     *
     * @param givenRanges
     * @return
     */
    private void sortRanges(final List<Range> givenRanges) {
        RangeSorter.getRangeSorter().sort(givenRanges, RANGE_COMPARATOR);
    }

    /**
     *
     * @param givenRanges
     * @return
     */
    private List<Range> findOverlappingRanges(final List<Range> givenRanges) {
        List<Range> newRange = new ArrayList<Range>();

        Range firstRange = givenRanges.get(0);
        int start = firstRange.getStart();
        int end = firstRange.getEnd();

        int i=1;
        do {
            Range next = givenRanges.get(i);
            if (next.getStart() <= end) {
                end = Math.max(next.getEnd(), end);
            } else {
                newRange.add(new Range(start, end));
                start = next.getStart();
                end = next.getEnd();
            }
        } while (++i<givenRanges.size());

        newRange.add(new Range(start, end));
        return newRange;
    }
}