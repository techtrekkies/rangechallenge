package com.wsi.range.compare;

import com.wsi.range.model.Range;

import java.util.Comparator;

public class RangeComparator implements Comparator<Range> {

    /**
     *
     * @param r1
     * @param r2
     * @return
     */
    public int compare(Range r1, Range r2) {
        if (r1==r2) return 0;
        if (r1==null && r2==null) return 0;
        if (r1==null) return 1;
        if (r2==null) return -1;

        return r1.getStart() - r2.getStart();
    }
}