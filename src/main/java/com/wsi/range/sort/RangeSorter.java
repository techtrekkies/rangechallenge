package com.wsi.range.sort;

import com.wsi.common.sort.ISorter;
import com.wsi.common.sort.SortFunction;
import com.wsi.range.compare.RangeComparator;
import com.wsi.range.model.Range;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
public class RangeSorter implements ISorter<Range> {

    private static final RangeSorter RANGE_SORTER
            = new RangeSorter((u,c) -> Collections.sort(u, c));

    private SortFunction sortFunction;

    public static RangeSorter getRangeSorter() {
        return RANGE_SORTER;
    }

    @Override
    public void sort(final List<Range> unsortedList, final Comparator<Range> rangeComparator) {
        sortFunction.performSort(unsortedList, rangeComparator);
    }

    protected RangeSorter(final SortFunction<Range> sortFunction) {
        this.sortFunction = sortFunction;
    }

}
