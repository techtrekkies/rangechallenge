package com.wsi.range.model;

/**
 *
 */
public class Range {
    private int start;
    private int end;

    public Range(int start, int end) {
        if (end<start) {
            throw new IllegalArgumentException("Invalid arguments: end < start");
        }
        this.start = start;
        this.end = end;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public String toString() {
        return "[" + start + "," + end + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Range range = (Range) o;

        if (getStart() != range.getStart()) return false;
        return getEnd() == range.getEnd();
    }

    @Override
    public int hashCode() {
        int result = getStart();
        result = 31 * result + getEnd();
        return result;
    }
}
