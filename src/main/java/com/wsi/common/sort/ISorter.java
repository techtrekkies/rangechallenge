package com.wsi.common.sort;

import java.util.Comparator;
import java.util.List;

public interface ISorter<T> {
    void sort(final List<T> unsortedList, final Comparator<T> tComparator);
}
