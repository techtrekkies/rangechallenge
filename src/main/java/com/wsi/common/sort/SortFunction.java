package com.wsi.common.sort;

import java.util.Comparator;
import java.util.List;

@FunctionalInterface
public interface SortFunction<T> {
    void performSort(final List<T> unsortedList, final Comparator<T> tComparator);
}
