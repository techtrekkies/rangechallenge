package com.wsi.common.helper;

import java.util.List;

public interface ListBasedHelper<T> {
    List<T> handle(final List<T> tList);
}
