package com.wsi.range.sort;

import com.wsi.range.compare.RangeComparator;
import com.wsi.range.model.Range;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Ramya on 3/10/2018.
 */
public class RangeSorterTest {

    private RangeComparator rangeComparator = new RangeComparator();

    private RangeSorter rangeSorter = RangeSorter.getRangeSorter();

    @Test
    public void sort_testForRangesSorted() throws Exception {
        Range[] ranges = new Range[] {
                new Range(94200,94299),
                new Range(94133,94133),
                new Range(94226, 94350),
                new Range(94275,94399)
        };

        List<Range> rangeList
                = Arrays.asList(ranges);
        rangeSorter.sort(rangeList, rangeComparator);

        Assert.assertTrue(rangeList.size()==4);
        Assert.assertTrue(rangeList.get(0).equals(ranges[0]));
    }
}