package com.wsi.range.helper;

import com.wsi.range.model.Range;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RangeHelperTest {

    private RangeHelper rangeHelper = new RangeHelper();

    @Test
    public void handle_testRange_noRangeMerges() throws Exception {
        Range[] ranges = new Range[] {
                new Range(94133,94133),
                new Range(94200,94299),
                new Range(94600,94699)
        };

        List<Range> testRanges = Arrays.asList(ranges);
        List<Range> newRange = rangeHelper.handle(testRanges);

        Assert.assertTrue(newRange.equals(testRanges));

        System.out.println("newRange:: " + newRange);
    }

    @Test
    public void handle_testRange_rangesMerged() throws Exception {
        Range[] ranges = new Range[] {
                new Range(94133,94133),
                new Range(94200,94299),
                new Range(94226, 94350),
                new Range(94275,94399)
        };

        List<Range> testRanges = Arrays.asList(ranges);
        List<Range> newRange = rangeHelper.handle(testRanges);

        Assert.assertFalse(newRange.equals(testRanges));
        Assert.assertTrue(newRange.size()==2);

        System.out.println("newRange:: " + newRange);
    }

    @Test
    public void handle_testRange_forNullRange() throws Exception {
        List<Range> newRange = rangeHelper.handle(null);
        Assert.assertTrue(newRange.size()==0);
        System.out.println("newRange:: " + newRange);
    }

    @Test
    public void handle_testRange_forSingleRange() throws Exception {
        Range[] ranges = new Range[] {
                new Range(94275,94399)
        };

        List<Range> testRanges = Arrays.asList(ranges);
        List<Range> newRange = rangeHelper.handle(testRanges);

        Assert.assertTrue(newRange.equals(testRanges));
        Assert.assertTrue(newRange.size()==1);

        System.out.println("newRange:: " + newRange);
    }

    @Test
    public void handle_testRange_forNegativeRange() throws Exception {
        Range[] ranges = new Range[] {
                new Range(-1000,1000),
                new Range(-10000, -5000),
                new Range(-500, 2500)

        };

        List<Range> testRanges = Arrays.asList(ranges);
        List<Range> newRange = rangeHelper.handle(testRanges);

        Assert.assertFalse(newRange.equals(testRanges));
        Assert.assertTrue(newRange.size()==2);

        System.out.println("newRange:: " + newRange);
    }


    @Test( expected = IllegalArgumentException.class)
    public void handle_testRange_invalidRange() throws Exception {
        Range[] ranges = new Range[] {
                new Range(94133,94132),
                new Range(94200,94299),
                new Range(94226, 94350),
                new Range(94275,94399)
        };

        List<Range> testRanges = Arrays.asList(ranges);
        List<Range> newRange = rangeHelper.handle(testRanges);

        Assert.assertFalse(newRange.equals(testRanges));
        Assert.assertTrue(newRange.size()==2);

        System.out.println("newRange:: " + newRange);
    }
}